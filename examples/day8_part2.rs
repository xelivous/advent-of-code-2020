use advent_of_code_2020::day8::Console;
use advent_of_code_2020::util::file_to_vec_string;

fn main() {
    let now = std::time::Instant::now();
    let args: Vec<String> = std::env::args().collect();

    let input_file = args
        .get(1)
        .expect("Please supply an input file as an argument");
    let lines = file_to_vec_string(input_file).expect("Please select a line-delimited input file");
    let mut console = Console::from_vec_string(&lines, 1).expect("Valid console");
    let result = console.run().expect("successful run");

    println!("Day 8 Part 2 || Answer: {:?}", result);
    println!("{:?}", now.elapsed());
}
