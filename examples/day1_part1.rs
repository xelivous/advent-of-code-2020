use advent_of_code_2020::{day1::find_two_nums_2020, util::file_to_vec_usize};

fn main() {
    let now = std::time::Instant::now();
    let args: Vec<String> = std::env::args().collect();

    let input_file = args
        .get(1)
        .expect("Please supply an input file as an argument");
    let lines = file_to_vec_usize(input_file).expect("Please select a line-delimited input file");

    match find_two_nums_2020(&lines) {
        Some((a, b)) => {
            assert_eq!(2020, a + b);
            println!(
                "Day 1 Part 1 || Found values: {}, {} || Answer: {}",
                a,
                b,
                a * b
            );
        }
        None => eprintln!("Could not find valid numbers in input file."),
    }

    println!("{:?}", now.elapsed());
}
