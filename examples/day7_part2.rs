use advent_of_code_2020::day7::{Bag, Bags};
use advent_of_code_2020::util::file_to_vec_string;

fn main() {
    let now = std::time::Instant::now();
    let args: Vec<String> = std::env::args().collect();

    let input_file = args
        .get(1)
        .expect("Please supply an input file as an argument");
    let lines = file_to_vec_string(input_file).expect("Please select a line-delimited input file");
    let bags = Bags::from_vec_string(&lines);
    let count = bags.count_required_bags(&Bag::new("shiny", "gold").get_key());

    println!("Day 7 Part 2 || Answer: {:?}", count);
    println!("{:?}", now.elapsed());
}
