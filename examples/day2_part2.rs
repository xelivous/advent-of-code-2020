use advent_of_code_2020::{day2::count_valid_passwords_part2, util::file_to_vec_string};

fn main() {
    let now = std::time::Instant::now();
    let args: Vec<String> = std::env::args().collect();

    let input_file = args
        .get(1)
        .expect("Please supply an input file as an argument");
    let lines = file_to_vec_string(input_file).expect("Please select a line-delimited input file");
    let num_valid = count_valid_passwords_part2(&lines);

    println!("Day 2 Part 2 || Answer: {}", num_valid);
    println!("{:?}", now.elapsed());
}
