use advent_of_code_2020::day4::{file_to_passport_vec, Passport};

fn main() {
    let now = std::time::Instant::now();
    let args: Vec<String> = std::env::args().collect();

    let input_file = args
        .get(1)
        .expect("Please supply an input file as an argument");
    let data = file_to_passport_vec(input_file).expect("Please select a valid input file");
    let mut valid_passports = 0;
    for passport_vec in data {
        if let Ok(pp) = Passport::load_from_vec_string(&passport_vec) {
            if pp.is_valid_passport(false) {
                valid_passports += 1;
            }
        }
    }

    println!("Day 4 Part 2 || Answer: {:?}", valid_passports);
    println!("{:?}", now.elapsed());
}
