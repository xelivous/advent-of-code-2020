use advent_of_code_2020::day3::{Map, Toboggan};

fn main() {
    let now = std::time::Instant::now();
    let args: Vec<String> = std::env::args().collect();

    let input_file = args
        .get(1)
        .expect("Please supply an input file as an argument");
    let map = Map::load_from_file(input_file).expect("Please select a valid input file");

    let mut sled = Toboggan::new(0, 0, 3, 1);
    let mut collisions = 0;

    loop {
        let (x, y) = sled.get_xy_on_map(&map);
        let tile = map.get_tile_at_pos(x, y);

        if sled.can_collide(tile) {
            //map.set_tile_at_pos(x, y, Tile::Collision);
            collisions += 1;
        } else {
            //map.set_tile_at_pos(x, y, Tile::NoCollision);
        }

        sled.step();

        if sled.is_below_map(&map) {
            break;
        }
    }

    println!("Day 3 Part 1 || Answer: {:?}", collisions);
    println!("{:?}", now.elapsed());
}
