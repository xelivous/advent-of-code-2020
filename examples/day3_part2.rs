use advent_of_code_2020::day3::{Map, Toboggan};

fn main() {
    let now = std::time::Instant::now();
    let args: Vec<String> = std::env::args().collect();

    let input_file = args
        .get(1)
        .expect("Please supply an input file as an argument");
    let map = Map::load_from_file(input_file).expect("Please select a valid input file");

    let trials = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
    let mut all_collisions: Vec<u64> = vec![];

    for trial in trials {
        let mut sled = Toboggan::new(0, 0, trial.0, trial.1);
        let mut collisions = 0;

        loop {
            let (x, y) = sled.get_xy_on_map(&map);
            let tile = map.get_tile_at_pos(x, y);

            if sled.can_collide(tile) {
                collisions += 1;
            }

            sled.step();

            if sled.is_below_map(&map) {
                break;
            }
        }

        all_collisions.push(collisions);
    }

    println!(
        "Day 3 Part 2 || Answer: {:?}",
        all_collisions.iter().product::<u64>()
    );
    println!("{:?}", now.elapsed());
}
