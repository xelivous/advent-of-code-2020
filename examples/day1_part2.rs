use advent_of_code_2020::{day1::find_three_nums_2020, util::file_to_vec_usize};

fn main() {
    let now = std::time::Instant::now();
    let args: Vec<String> = std::env::args().collect();

    let input_file = args
        .get(1)
        .expect("Please supply an input file as an argument");
    let lines = file_to_vec_usize(input_file).expect("Please select a line-delimited input file");

    match find_three_nums_2020(&lines) {
        Some((a, b, c)) => {
            assert_eq!(2020, a + b + c);
            println!(
                "Day 1 Part 2 || Found values: {}, {}, {} || Answer: {}",
                a,
                b,
                c,
                a * b * c
            );
        }
        None => eprintln!("Could not find valid numbers in input file."),
    }

    println!("{:?}", now.elapsed());
}
