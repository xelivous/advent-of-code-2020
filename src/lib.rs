pub type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

pub mod day1; //https://adventofcode.com/2020/day/1
pub mod day2; //https://adventofcode.com/2020/day/2
pub mod day3; //https://adventofcode.com/2020/day/3
pub mod day4; //https://adventofcode.com/2020/day/4

pub mod day7; //https://adventofcode.com/2020/day/7
pub mod day8; //https://adventofcode.com/2020/day/8

pub mod util; // The util module is only used for testing atm
