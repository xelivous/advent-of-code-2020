use crate::util::file_to_vec_string;
use crate::Result;

#[derive(Debug, Default)]
pub struct Toboggan {
    x: u64,
    y: u64,
    step_x: u64,
    step_y: u64,
}

impl Toboggan {
    pub fn new(x: u64, y: u64, step_x: u64, step_y: u64) -> Self {
        Toboggan {
            x,
            y,
            step_x,
            step_y,
        }
    }

    pub fn can_collide(&self, tile: &Tile) -> bool {
        match tile {
            Tile::Tree => true,
            _ => false,
        }
    }

    pub fn step(&mut self) {
        self.x += self.step_x;
        self.y += self.step_y;
    }

    pub fn get_index_on_map(&self, map: &Map) -> usize {
        map.get_tile_index(self.x, self.y)
    }

    pub fn get_xy_on_map(&self, map: &Map) -> (u64, u64) {
        map.index_to_pos(self.get_index_on_map(map))
    }

    pub fn is_below_map(&self, map: &Map) -> bool {
        self.y >= map.get_height()
    }
}

#[derive(Debug)]
pub enum Tile {
    Grass,
    Tree,
    Collision,
    NoCollision,
    Empty,
}
impl Tile {
    pub fn from_ascii(repr: &char) -> Tile {
        match repr {
            '#' => Tile::Tree,
            '.' => Tile::Grass,
            'O' => Tile::NoCollision,
            'X' => Tile::Collision,
            ' ' => Tile::Empty,
            _ => {
                eprintln!("Failed to parse ascii: {}", repr);
                Tile::Empty
            }
        }
    }
    pub fn to_ascii(&self) -> char {
        match self {
            Tile::Grass => '.',
            Tile::Tree => '#',
            Tile::Empty => ' ',
            Tile::Collision => 'X',
            Tile::NoCollision => 'O',
        }
    }
}

#[derive(Debug, Default)]
pub struct Map {
    data: Vec<Tile>,
    width: u64,
    height: u64,
}
impl Map {
    pub fn new() -> Self {
        Self {
            data: vec![],
            width: 0,
            height: 0,
        }
    }

    pub fn load_from_file(path: &str) -> Result<Self> {
        let input = file_to_vec_string(path)?;
        Ok(Map::load_from_vec_string(&input))
    }

    pub fn load_from_vec_string(input: &Vec<String>) -> Self {
        let mut data = vec![];
        let mut max_width = 0;

        for line in input {
            let mut temp_width = 0;

            for c in line.chars() {
                temp_width += 1;

                data.push(Tile::from_ascii(&c));
            }

            if temp_width > max_width {
                max_width = temp_width;
            }
        }

        Self {
            data,
            width: max_width,
            height: input.len() as u64,
        }
    }

    pub fn to_vec_string(&self) -> Vec<String> {
        let mut output = vec![];

        for y in 0..self.height {
            let mut line = String::new();
            for x in 0..self.width {
                let tile = self.get_tile_at_pos(x, y);
                line.push(tile.to_ascii())
            }
            output.push(line);
        }

        output
    }

    pub fn get_width(&self) -> u64 {
        self.width
    }

    pub fn get_height(&self) -> u64 {
        self.height
    }

    pub fn get_tile_index(&self, x: u64, y: u64) -> usize {
        let x = x % self.width;
        ((self.width * y) + x) as usize
    }

    pub fn get_tile_at_pos(&self, x: u64, y: u64) -> &Tile {
        let index = self.get_tile_index(x, y);
        match self.data.get(index) {
            Some(t) => t,
            None => {
                eprintln!(
                    "Tried to index {},{} in map, couldn't find it. Index: {}",
                    x, y, index
                );
                &Tile::Empty
            }
        }
    }

    pub fn set_tile_at_pos(&mut self, x: u64, y: u64, t: Tile) {
        let index = self.get_tile_index(x, y);
        if index < self.data.len() {
            self.data[index as usize] = t;
        }
    }

    pub fn index_to_pos(&self, index: usize) -> (u64, u64) {
        let x = index as u64 % self.width as u64;
        let y = index as u64 / self.width as u64;
        (x, y)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_example1() {
        let input = file_to_vec_string("assets/day3_example.txt").expect("needs input file");
        let map = Map::load_from_vec_string(&input);

        let mut sled = Toboggan::new(0, 0, 3, 1);
        let mut collisions = 0;

        loop {
            let (x, y) = sled.get_xy_on_map(&map);
            let tile = map.get_tile_at_pos(x, y);

            if sled.can_collide(tile) {
                collisions += 1;
            }

            sled.step();

            if sled.is_below_map(&map) {
                break;
            }
        }

        assert_eq!(7, collisions);
    }

    #[test]
    fn part2_example1() {
        let input = file_to_vec_string("assets/day3_example.txt").expect("needs input file");
        let map = Map::load_from_vec_string(&input);

        let trials = vec![(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)];
        let mut all_collisions = vec![];

        for trial in trials {
            let mut sled = Toboggan::new(0, 0, trial.0, trial.1);
            let mut collisions = 0;

            loop {
                let (x, y) = sled.get_xy_on_map(&map);
                let tile = map.get_tile_at_pos(x, y);

                if sled.can_collide(tile) {
                    collisions += 1;
                }

                sled.step();

                if sled.is_below_map(&map) {
                    break;
                }
            }

            all_collisions.push(collisions);
        }

        assert_eq!(336, all_collisions.iter().product());
    }
}
