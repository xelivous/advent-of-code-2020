/// A password that should be constrained by various rules
#[derive(Debug)]
struct Password {
    pub rule_min: usize,
    pub rule_max: usize,
    pub rule_char: char,
    pub input: String,
}

impl Password {
    /// Takes a specially formatted string and returns a Password
    pub fn new(data: &str) -> crate::Result<Self> {
        let (rule_min, rule_max, rule_char, input) = Password::parse_data(data)?;

        Ok(Self {
            rule_min,
            rule_max,
            rule_char,
            input,
        })
    }

    /// Parses a specially formatted string into its components.
    /// expects: /^(\d)-(\d) (\w): (\w+)$/
    fn parse_data(data: &str) -> crate::Result<(usize, usize, char, String)> {
        let split: Vec<&str> = data.split_whitespace().collect();

        let num_range = split.get(0).ok_or("data")?;
        let split2: Vec<&str> = num_range.split('-').collect();
        let rule_min = split2.get(0).ok_or("data")?;
        let rule_min = rule_min.parse::<usize>()?;
        let rule_max = split2.get(1).ok_or("data")?;
        let rule_max = rule_max.parse::<usize>()?;

        let rule_char = split.get(1).ok_or("data")?;
        let rule_char = rule_char.chars().nth(0).ok_or("data")?;

        let password: &str = split.get(2).ok_or("data")?;

        Ok((rule_min, rule_max, rule_char.into(), password.into()))
    }

    /// Check if the password is valid per the part1 rules.
    /// If password has between min and max of rule_char, it's valid.
    pub fn is_valid_part1(&self) -> bool {
        let mut occur = 0;
        for c in self.input.chars() {
            if c == self.rule_char {
                occur += 1;
            }
        }

        occur >= self.rule_min && occur <= self.rule_max
    }

    /// Check if the password is valid per the part2 rules.
    /// If password has one occurance between data[min-1] and data[max-2], it's valid.
    pub fn is_valid_part2(&self) -> bool {
        let mut occur = 0;
        let c: Vec<char> = self.input.chars().collect();

        if let Some(c) = c.get(self.rule_min - 1) {
            if c == &self.rule_char {
                occur += 1;
            }
        }
        if let Some(c) = c.get(self.rule_max - 1) {
            if c == &self.rule_char {
                occur += 1;
            }
        }

        occur == 1
    }
}

pub fn count_valid_passwords_part1(data: &Vec<String>) -> usize {
    let mut num_valid = 0;

    for line in data {
        if let Ok(password) = Password::new(line) {
            if password.is_valid_part1() {
                num_valid += 1;
            }
        }
    }

    num_valid
}

pub fn count_valid_passwords_part2(data: &Vec<String>) -> usize {
    let mut num_valid = 0;

    for line in data {
        if let Ok(password) = Password::new(line) {
            let valid = password.is_valid_part2();
            if valid {
                num_valid += 1;
            }
        }
    }

    num_valid
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_example1() -> crate::Result<()> {
        let input = vec![
            "1-3 a: abcde".into(),
            "1-3 b: cdefg".into(),
            "2-9 c: ccccccccc".into(),
        ];

        let num_valid = count_valid_passwords_part1(&input);

        assert_eq!(2, num_valid);

        Ok(())
    }

    #[test]
    fn part1_example2() -> crate::Result<()> {
        let input = vec![
            "1-3 a: abcde".into(),
            "1-3 b: cdefg".into(),
            "2-9 c: ccccccccc".into(),
        ];

        let num_valid = count_valid_passwords_part2(&input);

        assert_eq!(1, num_valid);

        Ok(())
    }
}
