use crate::Result;

#[derive(Debug, Default)]
pub struct Console {
    pc: usize,
    register: i64,
    program: Vec<Instruction>,
    max_instruction_visit: usize,
    visited_count: Vec<usize>,
}

impl Console {
    pub fn new(max_instruction_visit: usize) -> Self {
        Self {
            max_instruction_visit,
            ..Default::default()
        }
    }

    pub fn reset(&mut self, max_instruction_visit: usize) {
        self.visited_count = vec![0; self.program.len()];
        self.max_instruction_visit = max_instruction_visit;
        self.pc = 0;
        self.register = 0;
    }

    pub fn dump_program(&self) -> String {
        format!("{:#?}", self.program)
    }
    pub fn dump_visited(&self) -> String {
        format!("{:#?}", self.visited_count)
    }

    pub fn from_vec_string(input: &Vec<String>, max_instruction_visit: usize) -> Result<Self> {
        let mut console = Self::default();

        for line in input {
            console.program.push(Instruction::from_string(&line)?);
        }
        console.max_instruction_visit = max_instruction_visit;
        console.visited_count = vec![0; console.program.len()];

        Ok(console)
    }

    pub fn should_stop(&self) -> bool {
        self.visited_count
            .contains(&(self.max_instruction_visit + 1))
    }

    pub fn run_and_stop_on_duplicate_address(&mut self) -> Result<i64> {
        loop {
            self.visited_count[self.pc] += 1;
            if self.should_stop() {
                break;
            }
            let inst = self.program.get(self.pc);

            let inst = inst.ok_or("invalid address")?;

            let (pc, register) = inst.execute(self);

            self.pc = pc;
            self.register = register;
        }

        Ok(self.register)
    }

    pub fn swap_instr_with_nop(&mut self, pos: usize) {
        self.program[pos].0 = "nop".into();
    }

    pub fn swap_instr_with_jmp(&mut self, pos: usize) {
        self.program[pos].0 = "jmp".into();
    }

    pub fn swap_instr_opposite(&mut self, pos: usize) {
        self.program[pos].0 = match self.program[pos].0.as_str() {
            "nop" => "jmp".into(),
            "jmp" => "nop".into(),
            inst => inst.into(),
        }
    }

    pub fn find_infinite_loop_instrs(&mut self) -> Vec<usize> {
        loop {
            if self.pc >= self.program.len() {
                break;
            }

            self.visited_count[self.pc] += 1;
            if self.should_stop() {
                //break for now
                break;
            }

            let inst = self.program.get(self.pc);
            match inst {
                Some(inst) => {
                    let (pc, register) = inst.execute(self);
                    self.pc = pc;
                    self.register = register;
                }
                None => break,
            }
        }

        let mut instrs = vec![];
        for (pos, value) in self.visited_count.iter().enumerate() {
            if value == &self.max_instruction_visit {
                match self.program[pos].0.as_str() {
                    "nop" => instrs.push(pos),
                    "jmp" => instrs.push(pos),
                    _ => {}
                }
            }
        }
        instrs
    }

    pub fn run(&mut self) -> Result<i64> {
        let possible_instrs = self.find_infinite_loop_instrs();

        //loop through the possible instruction positions
        //swap them with the opposite instruction to see if it completes
        //reset the console back to the default values so we can attempt fresh every time
        for idx in possible_instrs {
            self.reset(1);
            self.swap_instr_opposite(idx);

            let mut valid_run = false;

            loop {
                if self.pc >= self.program.len() {
                    valid_run = true;
                    break;
                }
                self.visited_count[self.pc] += 1;

                if self.should_stop() {
                    //we exceeded our maximum instruction count for a pos, so it's invalid
                    valid_run = false;
                    break;
                }

                let inst = self.program.get(self.pc);
                match inst {
                    Some(inst) => {
                        let (pc, register) = inst.execute(self);
                        self.pc = pc;
                        self.register = register;
                    }
                    None => break,
                }
            }

            match valid_run {
                true => break,
                false => {
                    //ensure we set our program back to the original state
                    self.swap_instr_opposite(idx);
                    continue;
                }
            }
        }

        Ok(self.register)
    }
}

#[derive(Debug, Default)]
pub struct Instruction(String, i64);
impl Instruction {
    pub fn from_string(input: &str) -> Result<Self> {
        let split: Vec<&str> = input.split_whitespace().collect();
        let inst = split.get(0).ok_or("need instruction")?;
        let value = split.get(1).ok_or("need value")?;
        let value = value.parse::<i64>()?;
        Ok(Self(inst.to_string(), value))
    }

    pub fn execute(&self, console: &Console) -> (usize, i64) {
        let mut new_register = console.register;
        let mut new_pc = console.pc;
        match self.0.as_str() {
            "nop" => new_pc += 1,
            "acc" => {
                new_pc += 1;
                new_register += self.1;
            }
            "jmp" => {
                if self.1.is_negative() {
                    new_pc = new_pc.wrapping_sub(self.1.abs() as usize);
                } else {
                    new_pc = new_pc.wrapping_add(self.1 as usize);
                }
            }
            unknown => eprintln!("Unknown instruction: {:?} => {:?}", unknown, self),
        }

        (new_pc, new_register)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::util::file_to_vec_string;

    #[test]
    fn example_1() {
        let lines = file_to_vec_string("assets/day8_example.txt").expect("file");

        let mut console = Console::from_vec_string(&lines, 1).expect("Valid console");
        let result = console
            .run_and_stop_on_duplicate_address()
            .expect("successful run");
        assert_eq!(5, result);
    }

    #[test]
    fn example_2() {
        let lines = file_to_vec_string("assets/day8_example.txt").expect("file");

        let mut console = Console::from_vec_string(&lines, 1).expect("Valid console");
        let result = console.run().expect("success");
        assert_eq!(8, result);
    }
}
