use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::path::Path;

/// Converts a supplied whitespace delimited file into a vector of String
pub fn file_to_vec_string<P: AsRef<Path>>(path: P) -> crate::Result<Vec<String>> {
    let buf = BufReader::new(File::open(path)?);
    let mut lines = vec![];
    for line in buf.lines() {
        lines.push(line?);
    }

    Ok(lines)
}

/// Converts a supplied whitespace delimited file into a vector of usize
pub fn file_to_vec_usize<P: AsRef<Path>>(path: P) -> crate::Result<Vec<usize>> {
    let buf = BufReader::new(File::open(path)?);
    let mut lines = vec![];

    for line in buf.lines() {
        lines.push(line?.parse::<usize>()?);
    }

    Ok(lines)
}

pub fn vec_str_to_vec_string(v: Vec<&str>) -> Vec<String> {
    v.iter().map(|s| s.to_string()).collect()
}
