use crate::Result;
use std::collections::HashMap;

pub struct Bags {
    bags: HashMap<String, Bag>,
}
impl Bags {
    pub fn new() -> Self {
        Self {
            bags: HashMap::new(),
        }
    }

    pub fn from_vec_string(lines: &Vec<String>) -> Self {
        let mut bags = Bags::new();

        for line in lines {
            let bag = Bag::parse_line(&line).expect("valid parse");
            match bags.add_bag(bag) {
                Some(b) => panic!("Shouldn't have duplicate parent rules: {:?} ", b),
                None => {}
            };
        }

        bags
    }

    pub fn add_bag(&mut self, bag: Bag) -> Option<Bag> {
        self.bags.insert(bag.get_key(), bag)
    }

    pub fn reverse_bag_count(&self, search_key: &str) -> usize {
        let mut out = 0;
        for (_, parent_bag) in &self.bags {
            match self.recursive_bag_count(&parent_bag, search_key) {
                0 => {}
                _ => out += 1,
            }
        }
        out
    }

    pub fn recursive_bag_count(&self, bag: &Bag, search_key: &str) -> usize {
        let mut out = 0;

        for (child_key, child_count) in bag.children.iter() {
            if search_key == child_key {
                out += *child_count;
            } else {
                match self.bags.get(child_key) {
                    Some(c) => {
                        out += self.recursive_bag_count(&c, search_key);
                    }
                    None => {}
                }
            }
        }

        out
    }

    pub fn count_required_bags(&self, parent_key: &str) -> usize {
        let mut out = 0;

        let parent_bag = self.bags.get(parent_key).expect("valid key");

        for (child_key, child_count) in parent_bag.children.iter() {
            if let Some(child) = self.bags.get(child_key) {
                out += child_count * (1 + self.count_required_bags(&child.get_key()));
            } else {
                eprintln!("Could not find {}", child_key)
            }
        }

        out
    }
}
#[derive(Debug)]
pub struct Bag {
    color: String,
    adjective: String,
    children: HashMap<String, usize>,
}
impl Bag {
    pub fn new(adjective: &str, color: &str) -> Self {
        Self {
            adjective: adjective.into(),
            color: color.into(),
            children: HashMap::new(),
        }
    }

    pub fn parse_line(line: &str) -> Result<Bag> {
        let split: Vec<&str> = line.split("bags contain").collect();

        let parent: Vec<&str> = split[0].split(' ').map(|x| x.trim()).collect();
        let mut parent = Bag::new(&parent[0], &parent[1]);

        let children: Vec<&str> = split[1].split(',').map(|x| x.trim()).collect();
        for c in children {
            if c == "no other bags." {
                continue;
            }
            let child: Vec<&str> = c.split(' ').map(|x| x.trim()).collect();
            let child_count = child[0].parse::<usize>()?;
            let child = Bag::new(&child[1], &child[2]);
            parent.children.insert(child.get_key(), child_count);
        }

        Ok(parent)
    }

    pub fn get_key(&self) -> String {
        format!("{}_{}", self.adjective, self.color)
    }

    pub fn contains_child(&self, key: &str) -> bool {
        self.children.contains_key(key)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::util::file_to_vec_string;

    #[test]
    fn parse_bag_line() {
        let lines = file_to_vec_string("assets/day7_example1.txt").expect("file");

        for line in lines {
            let _ = Bag::parse_line(&line).expect("valid line");
        }
    }

    #[test]
    fn example1() {
        let lines = file_to_vec_string("assets/day7_example1.txt").expect("file");

        let bags = Bags::from_vec_string(&lines);
        let count = bags.reverse_bag_count(&Bag::new("shiny", "gold").get_key());

        assert_eq!(4, count);
    }

    #[test]
    fn example2() {
        let lines = file_to_vec_string("assets/day7_example2.txt").expect("file");

        let bags = Bags::from_vec_string(&lines);
        let count = bags.count_required_bags(&Bag::new("shiny", "gold").get_key());

        assert_eq!(126, count);
    }
}
