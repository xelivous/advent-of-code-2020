use crate::Result;
use std::path::Path;

#[derive(Debug, Default)]
pub struct Passport {
    pub birth_year: Option<BirthYear>,
    pub issue_year: Option<IssueYear>,
    pub expiration_year: Option<ExpirationYear>,
    pub height: Option<Height>,
    pub hair_color: Option<HairColor>,
    pub eye_color: Option<EyeColor>,
    pub passport_id: Option<PassportID>,
    pub country_id: Option<CountryID>,
}
impl Passport {
    pub fn load_from_vec_string(data: &Vec<String>) -> Result<Self> {
        let mut pp = Self::default();

        for point in data {
            let entry: Vec<String> = point.split(":").map(|x| x.to_owned()).collect();
            let key = match entry.get(0) {
                Some(k) => k,
                None => continue,
            };
            let value = match entry.get(1) {
                Some(k) => k,
                None => continue,
            };

            match key.as_str() {
                "byr" => pp.birth_year = BirthYear(value.parse::<u64>()?).into(),
                "iyr" => pp.issue_year = IssueYear(value.parse::<u64>()?).into(),
                "eyr" => pp.expiration_year = ExpirationYear(value.parse::<u64>()?).into(),
                "hgt" => pp.height = Height(value.into()).into(),
                "hcl" => pp.hair_color = HairColor(value.into()).into(),
                "ecl" => pp.eye_color = EyeColor(value.into()).into(),
                "pid" => pp.passport_id = PassportID(value.into()).into(),
                "cid" => pp.country_id = CountryID(value.into()).into(),
                _ => {}
            }
        }

        Ok(pp)
    }

    pub fn is_valid_passport(&self, ignore_requirements: bool) -> bool {
        let byr = match &self.birth_year {
            Some(b) => ignore_requirements || b.matches_requirements(),
            None => !BirthYear::is_required(),
        };

        let iyr = match &self.issue_year {
            Some(b) => ignore_requirements || b.matches_requirements(),
            None => !IssueYear::is_required(),
        };

        let eyr = match &self.expiration_year {
            Some(b) => ignore_requirements || b.matches_requirements(),
            None => !ExpirationYear::is_required(),
        };

        let hgt = match &self.height {
            Some(b) => ignore_requirements || b.matches_requirements(),
            None => !Height::is_required(),
        };

        let hcl = match &self.hair_color {
            Some(b) => ignore_requirements || b.matches_requirements(),
            None => !HairColor::is_required(),
        };

        let ecl = match &self.eye_color {
            Some(b) => ignore_requirements || b.matches_requirements(),
            None => !EyeColor::is_required(),
        };

        let pid = match &self.passport_id {
            Some(b) => ignore_requirements || b.matches_requirements(),
            None => !PassportID::is_required(),
        };

        let cid = match &self.country_id {
            Some(b) => ignore_requirements || b.matches_requirements(),
            None => !CountryID::is_required(),
        };

        byr && iyr && eyr && hgt && hcl && ecl && pid && cid
    }
}

pub trait PassportField {
    fn text_repr() -> String;
    fn is_required() -> bool;
    fn matches_requirements(&self) -> bool;
}

#[derive(Debug, Default)]
pub struct BirthYear(u64);
impl PassportField for BirthYear {
    fn text_repr() -> String {
        "byr".into()
    }

    fn is_required() -> bool {
        true
    }

    fn matches_requirements(&self) -> bool {
        self.0 >= 1920 && self.0 <= 2002
    }
}

#[derive(Debug, Default)]
pub struct IssueYear(u64);
impl PassportField for IssueYear {
    fn text_repr() -> String {
        "iyr".into()
    }

    fn is_required() -> bool {
        true
    }

    fn matches_requirements(&self) -> bool {
        self.0 >= 2010 && self.0 <= 2020
    }
}
#[derive(Debug, Default)]
pub struct ExpirationYear(u64);
impl PassportField for ExpirationYear {
    fn text_repr() -> String {
        "eyr".into()
    }

    fn is_required() -> bool {
        true
    }

    fn matches_requirements(&self) -> bool {
        self.0 >= 2020 && self.0 <= 2030
    }
}
#[derive(Debug, Default)]
pub struct Height(String);
impl PassportField for Height {
    fn text_repr() -> String {
        "hgt".into()
    }

    fn is_required() -> bool {
        true
    }

    fn matches_requirements(&self) -> bool {
        if let Some(idx) = self.0.find("cm") {
            let split = self.0.split_at(idx);

            match split.0.parse::<u64>() {
                Ok(num) => return num >= 150 && num <= 193,
                Err(_) => return false,
            }
        }
        if let Some(idx) = self.0.find("in") {
            let split = self.0.split_at(idx);

            match split.0.parse::<u64>() {
                Ok(num) => return num >= 59 && num <= 76,
                Err(_) => return false,
            }
        }

        false
    }
}
#[derive(Debug, Default)]
pub struct HairColor(String);
impl PassportField for HairColor {
    fn text_repr() -> String {
        "hcl".into()
    }

    fn is_required() -> bool {
        true
    }

    fn matches_requirements(&self) -> bool {
        if self.0.len() != 7 {
            return false;
        }

        let chars: Vec<char> = self.0.chars().collect();
        if chars[0] != '#' {
            return false;
        }

        for c in 1..chars.len() {
            if chars[c].is_digit(10) || (chars[c].is_ascii_hexdigit() && chars[c].is_lowercase()) {
            } else {
                return false;
            }
        }

        true
    }
}
#[derive(Debug, Default)]
pub struct EyeColor(String);
impl PassportField for EyeColor {
    fn text_repr() -> String {
        "ecl".into()
    }

    fn is_required() -> bool {
        true
    }

    fn matches_requirements(&self) -> bool {
        match self.0.as_str() {
            "amb" => true,
            "blu" => true,
            "brn" => true,
            "gry" => true,
            "grn" => true,
            "hzl" => true,
            "oth" => true,
            _ => false,
        }
    }
}
#[derive(Debug, Default)]
pub struct PassportID(String);
impl PassportField for PassportID {
    fn text_repr() -> String {
        "pid".into()
    }

    fn is_required() -> bool {
        true
    }

    fn matches_requirements(&self) -> bool {
        self.0.len() == 9 && self.0.parse::<u64>().is_ok()
    }
}
#[derive(Debug, Default)]
pub struct CountryID(String);
impl PassportField for CountryID {
    fn text_repr() -> String {
        "cid".into()
    }

    fn is_required() -> bool {
        false
    }

    fn matches_requirements(&self) -> bool {
        true
    }
}

pub fn file_to_passport_vec<P: AsRef<Path>>(path: P) -> Result<Vec<Vec<String>>> {
    let data = std::fs::read_to_string(path)?;
    let data: Vec<Vec<String>> = data
        .split("\n\n")
        .map(|x| {
            x.split_whitespace()
                .map(|x| x.to_owned())
                .collect::<Vec<_>>()
        })
        .collect();

    Ok(data)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1_example1() {
        let data = file_to_passport_vec("assets/day4_example.txt").expect("file");

        let mut valid_passports = 0;
        for passport_vec in data {
            if let Ok(pp) = Passport::load_from_vec_string(&passport_vec) {
                if pp.is_valid_passport(true) {
                    valid_passports += 1;
                }
            }
        }

        assert_eq!(2, valid_passports);
    }

    #[test]
    fn part2_example_valid() {
        let data = file_to_passport_vec("assets/day4_example_valid.txt").expect("file");

        let mut valid_passports = 0;
        for passport_vec in data {
            if let Ok(pp) = Passport::load_from_vec_string(&passport_vec) {
                if pp.is_valid_passport(false) {
                    valid_passports += 1;
                }
            }
        }

        assert_eq!(4, valid_passports);
    }

    #[test]
    fn part2_example_invalid() {
        let data = file_to_passport_vec("assets/day4_example_invalid.txt").expect("file");

        let mut valid_passports = 0;
        for passport_vec in data {
            if let Ok(pp) = Passport::load_from_vec_string(&passport_vec) {
                if pp.is_valid_passport(false) {
                    valid_passports += 1;
                }
            }
        }

        assert_eq!(0, valid_passports);
    }

    #[test]
    fn byr_inputs() {
        assert_eq!(true, BirthYear(2002).matches_requirements());
        assert_eq!(false, BirthYear(2003).matches_requirements());
    }

    #[test]
    fn hgt_inputs() {
        assert_eq!(true, Height("60in".into()).matches_requirements());
        assert_eq!(true, Height("190cm".into()).matches_requirements());
        assert_eq!(false, Height("190in".into()).matches_requirements());
        assert_eq!(false, Height("190".into()).matches_requirements());
    }

    #[test]
    fn hcl_inputs() {
        assert_eq!(true, HairColor("#123abc".into()).matches_requirements());
        assert_eq!(false, HairColor("#123abz".into()).matches_requirements());
        assert_eq!(false, HairColor("123abc".into()).matches_requirements());
    }
}
