/// Finds 2 numbers in the supplied vec that add up to 2020
/// Returns them sorted from lowest to highest
pub fn find_two_nums_2020(data: &Vec<usize>) -> Option<(usize, usize)> {
    let mut input = data.clone();
    input.retain(|x| x <= &2020); // remove anything larger than 2020, if there were any.
    input.sort_unstable(); // sort it because reasons

    for a in 0..input.len() {
        for b in (0..input.len()).rev() {
            if input[a] + input[b] == 2020 {
                //sort the results so it's easier to test
                let mut res = vec![input[a], input[b]];
                res.sort_unstable();
                return Some((res[0], res[1]));
            }
        }
    }

    None
}

/// Finds 3 numbers in the supplied vec that add up to 2020
/// Returns them sorted from lowest to highest
pub fn find_three_nums_2020(data: &Vec<usize>) -> Option<(usize, usize, usize)> {
    let mut input = data.clone();
    input.retain(|x| x <= &2020); // remove anything larger than 2020, if there were any.
    input.sort_unstable(); // sort it because reasons

    let len = input.len();

    for a in 0..len {
        for b in (0..len).rev() {
            for c in (len / 2..len).chain(0..len) {
                if input[a] + input[b] + input[c] == 2020 {
                    //sort the results so it's easier to test
                    let mut res = vec![input[a], input[b], input[c]];
                    res.sort_unstable();
                    return Some((res[0], res[1], res[2]));
                }
            }
        }
    }

    None
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic]
    fn test_twonum_empty() {
        let input = vec![];
        let (_a, _b) = find_two_nums_2020(&input).expect("Should have a valid match");
    }

    #[test]
    fn test_twonum_minimal() {
        let input = vec![2020, 0];
        let (a, b) = find_two_nums_2020(&input).expect("Should have a valid match");
        assert_eq!(2020, a + b);
        assert_eq!(0, a * b);

        let input = vec![0, 2020];
        let (a, b) = find_two_nums_2020(&input).expect("Should have a valid match");
        assert_eq!(2020, a + b);
        assert_eq!(0, a * b);

        let input = vec![1010, 1010];
        let (a, b) = find_two_nums_2020(&input).expect("Should have a valid match");
        assert_eq!(2020, a + b);
        assert_eq!(1020100, a * b);

        let input = vec![2019, 1];
        let (a, b) = find_two_nums_2020(&input).expect("Should have a valid match");
        assert_eq!(2020, a + b);
        assert_eq!(2019, a * b);
    }

    #[test]
    fn part1_example1() -> crate::Result<()> {
        let input = vec![1721, 979, 366, 299, 675, 1456];

        let (a, b) = find_two_nums_2020(&input).expect("Should have a valid match");
        assert_eq!(2020, a + b);
        assert_eq!(299, a);
        assert_eq!(1721, b);
        assert_eq!(514579, a * b);

        Ok(())
    }

    #[test]
    fn part2_example1() -> crate::Result<()> {
        let input = vec![1721, 979, 366, 299, 675, 1456];

        let (a, b, c) = find_three_nums_2020(&input).expect("Should have a valid match");
        assert_eq!(2020, a + b + c);
        assert_eq!(366, a);
        assert_eq!(675, b);
        assert_eq!(979, c);
        assert_eq!(241861950, a * b * c);

        Ok(())
    }
}
