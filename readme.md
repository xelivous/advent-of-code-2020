Advent of Code 2020
===

Basic rust implementations of AoC2020 trying to not use dependencies. Might need to use them later, who knows.

Running Examples
---

Use something similar to the following:

```sh
cargo run --example day1_part1 -- ./assets/day1.txt
```

where `./assets/day1.txt` if the input for day1
